import pandas as pd
import numpy as np

path = r'C:\Users\Documents\University\ML\Lab4\\'

my_dataframe = pd.read_csv(path + 'HousePrice.csv')
# Drop cells with any empty rows
my_dataframe = my_dataframe.dropna(axis=1)
# Save to file
my_dataframe.to_csv(path + 'HousePriceValid.csv')


# Prepare two tables
splitFirst = np.split(my_dataframe,[500], axis=0)
splitSecond = np.split(splitFirst[1],[500], axis=0)

first = splitFirst[0].append(splitSecond[1])
second = splitSecond[0].append(splitSecond[1])

firstTable = first[['Id','SalePrice','Street']]
secondTable = second[['Id','YrSold','MoSold']]

# firstTable.to_csv(path + 'FirstTable.csv')
# secondTable.to_csv(path + 'SecondTable.csv')


#inner join in python pandas
print("Inner Join:\n", pd.merge(firstTable, secondTable, on='Id', how='inner'))

# outer join in python pandas
print("Outer Join:\n",pd.merge(firstTable, secondTable, on='Id', how='outer'))

# left join in python
print("Left Join:\n",pd.merge(firstTable, secondTable, on='Id', how='left'))

# right join in python pandas
print("Right Join:\n",pd.merge(firstTable, secondTable, on='Id', how='right'))

SalePrice = firstTable['SalePrice']
print("Sum:", SalePrice.sum())
print("Count:", SalePrice.count())
print("Mean:", SalePrice.mean())
print("Median:", SalePrice.median())
print("Max:", SalePrice.max())
print("Min:", SalePrice.min())
print("STD:", SalePrice.std())